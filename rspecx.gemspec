# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rspecx/version'

Gem::Specification.new do |spec|
  spec.name          = 'rspecx'
  spec.version       = Rspecx::VERSION
  spec.authors       = ['Matt Turner']
  spec.email         = ['matthew.turner@sage.com']
  spec.summary       = %q{Adds improved command line support for rspec}
  spec.description   = %q{Parses out previous test runs and copies paths to clipboard}
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(spec|features)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'thor'
  spec.add_dependency 'rspec'

  spec.add_development_dependency 'bundler', '~> 1.6'
  spec.add_development_dependency 'rake'
end
