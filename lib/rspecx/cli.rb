require 'thor'

module Rspecx
  class Cli < Thor
    OUTPUT_FILE_PATH = '/tmp/rspec.txt'

    desc 'r(index)', 'Runs rspec and stores the result or runs the spec with the index from the l command'
    def r(index = nil)
      if index
        h = hashify_specs
        spec_path = h[index.to_i]
        execute_spec(spec_path)
      else
        execute_specs
      end
    end

    desc 'c', 'Copies the path to the failing spec'
    def c(index)
      h = hashify_specs
      system "echo #{h[index.to_i]} | xclip -i -selection clipboard > /dev/null"
    end

    desc 'l', 'Lists all the failing rspec tests'
    def l
      rspec_root = File.open(OUTPUT_FILE_PATH, &:readline)
      puts "Failing rspecs from #{rspec_root}"
      puts ''
      failing_specs do |index, spec_path|
        puts "\t#{index}:\t#{spec_path}"
      end
      puts ''
    end

    desc 'o', 'Outputs last test run'
    def o
      File.open(OUTPUT_FILE_PATH).each do |line|
        print line
      end
    end

    private
    def hashify_specs
      h = {}
      failing_specs do |index, spec_path|
        h[index] = spec_path
      end
      h
    end

    def failing_specs
      index = 1
      File.open(OUTPUT_FILE_PATH) do |f|
        f.each_line.detect do |line|
          match = /^rspec.* #/.match(line)
          if match
            spec_path = match.to_s[8..-2]
            yield index, spec_path
          end
        end
      end
    end

    def execute_spec(spec_path)
      IO.popen("bundle exec rspec #{spec_path}") do |f|
        until f.eof?
          bit = f.getc
          $stdout.putc bit
        end
      end
    end

    def execute_specs
      File.delete OUTPUT_FILE_PATH if File.exist? OUTPUT_FILE_PATH
      File.open(OUTPUT_FILE_PATH, 'w') do |file|
        file.puts File.basename(Dir.pwd)
        IO.popen('bundle exec rspec') do |f|
          until f.eof?
            bit = f.getc
            $stdout.putc bit
            file.write(bit)
          end
        end
      end
    end
  end
end